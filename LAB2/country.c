/*
 *  Задание #6
 *  Автор: 
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "map.h"

void undecorate_name(char* name)
{
    while (*name != '\0')
    {
        if (*name == '_')
        {
            *name = ' ';
        }
        ++name;
    }
}

int main(int argc, char * argv[])
{
    COUNTRY** map;
    map = map_load();

    int err = 0;

    while (1)
    {
        printf(">>");

        char cmd[10];
        scanf("%s", cmd);

        if (!strcmp(cmd, "add"))
        {
            char name[40];
            scanf("%s", name);
            undecorate_name(name);

            int population;
            scanf("%d", &population);

            int area;
            scanf("%d", &area);

            map_add(map, name, population, area);
        }
        else if (!strcmp(cmd, "delete"))
        {
            char name[40];
            scanf("%s", name);
            undecorate_name(name);

            map_delete(map, name);
        }
        else if (!strcmp(cmd, "dump"))
        {
            map_dump(map);
        }
        else if (!strcmp(cmd, "view"))
        {
            char name[40];
            scanf("%s", name);
            undecorate_name(name);

            COUNTRY* p = map_find(map, name);
            if (!p)
            {
                printf("country doesn't exists\n");
                err = 1;
            }
            else
            {
                print_country(p);
            }
        }
        else if (!strcmp(cmd, "count"))
        {
            printf("%d\n", map_count(map));
        }
        else if (!strcmp(cmd, "save"))
        {
            map_save(map);
        }
        else if (!strcmp(cmd, "quit"))
        {
            break;
        }
        else
        {
            printf("command not found\n");
        }
    }

    map_save(map);
    map_clear(map);

    return err;
}
